class Node:
    # Узел для двусвязанного списка
    def __init__(self, data):
        self.item = data
        self.next_ref = None
        self.prev_ref = None


class DoubleLinkedList:
    def __init__(self):
        self.start_node = None

    def insert_in_empty_list(self, data):
        # Вставка элементов в пустой список
        if not self.start_node:
            new_node = Node(data)
            self.start_node = new_node
        else:
            print("The list is not empty")

    def insert_at_start(self, data):
        # вставка элементов в начало
        if not self.start_node:
            new_node = Node(data)
            self.start_node = new_node
            return print("node inserted")
        new_node = Node(data)
        new_node.next_ref = self.start_node
        self.start_node.prev_ref = new_node
        self.start_node = new_node

    def add_element(self, data):
        # добавление элементов в конце
        if not self.start_node:
            new_node = Node(data)
            self.start_node = new_node
            return
        element = self.start_node
        while element.next_ref:
            element = element.next_ref
        new_node = Node(data)
        element.next_ref = new_node
        new_node.prev_ref = element

    def insert_after_item(self, after_item, data):
        # вставка элемента после индекса
        if not self.start_node:
            print("List is empty")
            return
        else:
            element = self.start_node
            while element:
                if element.item == after_item:
                    break
                element = element.next_ref
            if not element:
                print("item not in the list")
            else:
                new_node = Node(data)
                new_node.prev_ref = element
                new_node.next_ref = element.next_ref
                if element.next_ref:
                    element.next_ref.prev_ref = new_node
                element.next_ref = new_node

    def insert_before_item(self, before_item, data):
        # вставка элемента перед индексом
        if not self.start_node:
            return print("List is empty")
        else:
            element = self.start_node
            while element is not None:
                if element.item == before_item:
                    break
                element = element.next_ref
            if not element:
                print("item not in the list")
            else:
                new_node = Node(data)
                new_node.next_ref = element
                new_node.prev_ref = element.prev_ref
                if element.prev_ref:
                    element.prev_ref.next_ref = new_node
                element.prev_ref = new_node

    def delete_at_end(self):
        # удаление элементов из конца
        if not self.start_node:
            print("The list has no element to delete")
            return
        if not self.start_node.next_ref:
            self.start_node = None
            return
        element = self.start_node
        while element.next_ref:
            element = element.next_ref
        element.prev_ref.next_ref = None

    def delete_element_by_value(self, value):
        # удаление элементов по значению
        if not self.start_node:
            print("The list has no element to delete")
            return
        if not self.start_node.next_ref:
            if self.start_node.item == value:
                self.start_node = None
            else:
                print("Item not found")
            return
        if self.start_node.item == value:
            self.start_node = self.start_node.next_ref
            self.start_node.prev_ref = None
            return
        element = self.start_node
        while element.next_ref:
            if element.item == value:
                break
            element = element.next_ref
        if element.next_ref:
            element.prev_ref.next_ref = element.next_ref
            element.next_ref.prev_ref = element.prev_ref
        else:
            if element.item == value:
                element.prev_ref.next_ref = None
            else:
                print("Element not found")

    def reverse_linked_list(self):
        # реверсирование двусвязного списка
        if not self.start_node:
            return print("The list has no element to delete")
        p = self.start_node
        q = p.next_ref
        p.next_ref = None
        p.prev_ref = q
        while q:
            q.prev_ref = q.next_ref
            q.next_ref = p
            p = q
            q = q.prev_ref
        self.start_node = p

    def get_list(self):
        # обход двусвязного списка
        if not self.start_node:
            print("List has no element")
            return
        else:
            result = '['
            element = self.start_node
            while element:
                result += element.item + ', '
                element = element.next_ref
            print(result[:-2]+']')


if __name__ == "__main__":
    new_linked_list = DoubleLinkedList()
    new_linked_list.add_element('Car')
    new_linked_list.add_element('Apple')
    new_linked_list.insert_at_start('Ball')
    new_linked_list.insert_before_item('Apple', 'Strawberry')
    new_linked_list.get_list()                                # [Ball, Car, Strawberry, Apple]
    new_linked_list.reverse_linked_list()
    new_linked_list.get_list()                                # [Apple, Strawberry, Car, Ball]
    new_linked_list.insert_at_start('Cup')
    new_linked_list.insert_after_item('Ball', 'Coin')
    new_linked_list.get_list()                                # [Cup, Apple, Strawberry, Car, Ball, Coin]
    new_linked_list.insert_in_empty_list('Bag')               # "The list is not empty"
    new_linked_list.delete_at_end()
    new_linked_list.get_list()                                # [Cup, Apple, Strawberry, Car, Ball]
    new_linked_list.delete_element_by_value('Car')
    new_linked_list.get_list()                                # [Cup, Apple, Strawberry, Ball]