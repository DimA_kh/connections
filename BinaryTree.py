class Node:
    """Класс узла двоичного дерева"""

    def __init__(self, element, left_child=None, right_child=None):
        self.element = element
        self.left_child = left_child
        self.right_child = right_child


class Tree:
    """Класс дерева"""

    def __init__(self, root=None):
        self.root = root

    def add(self, element):
        """Добавить узлы в дерево"""
        node = Node(element)
        # Если дерево пусто, присвоить значение корневому узлу
        if not self.root:
            self.root = node
            return
        queue = [self.root]
        while queue:
            # Список Python вместо очереди
            cur_node = queue.pop(0)
            if not cur_node.left_child:
                cur_node.left_child = node
                return
            else:
                queue.append(cur_node.left_child)
            if not cur_node.right_child:
                cur_node.right_child = node
                return
            else:
                queue.append(cur_node.right_child)

    def pre_order(self, root):
        """Рекурсивная реализация прямого обхода дерева.
        Выводит родителя до всех его потомков"""
        if not root:
            return
        print(root.element, end=' ')
        self.pre_order(root.left_child)
        self.pre_order(root.right_child)

    def post_order(self, root):
        """Рекурсивная реализация обратного обхода дерева.
        Выводит потомков, а затем родителя"""
        if not root:
            return
        self.post_order(root.left_child)
        self.post_order(root.right_child)
        print(root.element, end=' ')

    def in_order(self, root):
        """Рекурсивная реализация центрированного обхода дерева.
        Выводит левого потомка, затем родителя, затем правого потомка"""
        if not root:
            return
        self.in_order(root.left_child)
        print(root.element, end=' ')
        self.in_order(root.right_child)

    def breadth_travel(self):
        """Использование очередей для обхода дерева в ширину"""
        if not self.root:
            return
        queue = []
        queue.append(self.root)
        while queue:
            node = queue.pop(0)
            print(node.element, end=' ')
            if node.left_child:
                queue.append(node.left_child)
            if node.right_child:
                queue.append(node.right_child)

    def depth_travel(self):
        """Использование очередей для обхода дерева в глубину"""
        queue = []
        queue.append(self.root)
        while queue:
            node = queue.pop()
            print(node.element, end=' ')
            if node.right_child:
                queue.append(node.right_child)
            if node.left_child:
                queue.append(node.left_child)


'''Создание и простой обход дерева'''
if __name__ == "__main__":
    tree = Tree()
    tree.add(0)
    tree.add(1)
    tree.add(2)
    tree.add(3)
    tree.add(4)
    tree.add(5)
    tree.add(6)
    tree.add(7)
    tree.add(8)
    tree.add(9)
    print('Прямой обход дерева (Pre-Order)')
    tree.pre_order(tree.root)
    print('\n')
    print('Центрированный обход дерева (In-Order)')
    tree.in_order(tree.root)
    print('\n')
    print('Обратный обход дерева (Post-Order)')
    tree.post_order(tree.root)
    print('\n')
    print('Обход в глубину')
    tree.depth_travel()
    print('\n')
    print('Обход в ширину')
    tree.breadth_travel()
