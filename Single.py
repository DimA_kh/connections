class Node():
    # Узел для связанного списка
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList():

    def __init__(self):
        self.head = None
        self.end = None

    def length(self):
        '''Возвращает длину связанного списка'''
        current_node = self.head
        node_total = 0
        while current_node != None:
            node_total += 1
            current_node = current_node.next
        return node_total

    def is_empty_list(self):
        '''Проверяет, является ли связанный список пустым'''
        return self.head == None

    def stack_insert(self, data):
        '''Вставляет узел в HEAD списка;
        LIFO (последним пришел, первым ушел)'''
        new_node = Node(data)
        # устанавливаем new_node.next указывать на следующий узел (по умолчанию None, если 1-я вставка)
        new_node.next = self.head
        # устанавливаем HEAD связанного списка указывать на новый узел
        self.head = new_node
        if self.end == None:
            # Первая вставка в связанный список; узел становится HEAD & END
            self.end = new_node

    def queue_append(self, data):
        '''Добавляет узел в END списка;
           FIFO (первым пришел, первым ушел).'''
        new_node = Node(data)
        if self.head == None:
            # Первая вставка в связанный список; узел становится HEAD & END
            self.head = self.end = new_node
        else:
            # Делает исходный последний узел точкой к недавно добавленному узлу
            self.end.next = new_node
            # Делает недавно добавленный список официальным END связанного списка
            self.end = new_node

    def insert_after(self, data, index):
        '''Вставляет НОВЫЙ узел ПОСЛЕ определенного узла, указанного индексом'''
        # Необходимо убедиться, что предоставленный индекс находится в пределах допустимого диапазона
        if index < 0:
            return print("ERROR: 'insert_after' Index cannot be negative!")
        elif index > self.length() - 1:
            return print("ERROR: 'insert_after' Index exceeds linked list range!")
        # Используйте queue_insert для добавления в качестве последнего элемента
        elif index == self.length() - 1 or self.is_empty_list():
            self.queue_append(data)
            return
        new_node = Node(data)
        current_node = self.head
        search_index = 0
        # Продолжайте проходить через узлы до нужного узла
        while search_index != index:
            current_node = current_node.next
            search_index += 1
        # Делает предыдущий узел указывающим на целевой узел
        prior_node = current_node
        # Заставляет текущий узел указывать на узел ПОСЛЕ целевого узла (по умолчанию нет последнего узла)
        current_node = current_node.next
        # Заставляет целевой узел указывать на недавно добавленный узел
        prior_node.next = new_node
        # Заставляет вновь добавленный узел указывать на исходный узел, который БЫЛ ПОСЛЕ целевого узла
        new_node.next = current_node

    def delete_head(self):
        '''Удаляет узел HEAD'''
        # Связанный список пуст
        if self.is_empty_list():
            return
        # Настраивает указатель головы, чтобы пройти мимо оригинальной HEAD
        self.head = self.head.next
        if self.head == None:
            # Единственный узел только что был удален
            self.end = None

    def delete_end(self):
        '''Удаляет узел END'''
        # Связанный список пуст
        if self.is_empty_list():
            return
        prior_node = self.head
        current_node = self.head
        # Необходимо продолжать цикл, пока не будет достигнут последний узел
        while current_node.next != None:
            prior_node = current_node
            current_node = current_node.next
        # Настройте новый узел END так, чтобы он ни на что не указывал
        prior_node.next = None
        # Заставляет связанный список забыть об исходном узле END
        self.end = prior_node

    def delete_node(self, index):
        '''Удаляет целевой узел через index'''
        # Связанный список пуст
        if self.is_empty_list():
            return
        # Необходимо убедиться, что индекс находится в правильном диапазоне
        elif index < 0:
            return print("ERROR: 'delete_node' Index cannot be negative!")
        elif index > self.length() - 1:
            return print("ERROR: 'delete_node' Index exceeds linked list range")
        prior_node = self.head
        current_node = self.head
        search_index = 0
        if index == 0:
            self.head = self.head.next
            return
        # Продолжайте обход узлов до нужного узла
        while search_index != index:
            prior_node = current_node
            current_node = current_node.next
            search_index += 1
        # Настраивает узел ДО целевого узла, чтобы он указывал на узел ПОСЛЕ целевого узла
        prior_node.next = current_node.next

    def update_node(self, new_data, index):
        '''Обновляет данные целевого узла через index'''
        # Связанный список пуст
        if self.is_empty_list():
            return print("ERROR: 'update_node' Linked list is empty; no node data to update!")
        # Необходимо убедиться, что индекс находится в правильном диапазоне
        elif index < 0:
            return print("ERROR: 'update_node' Index cannot be negative!")
        elif index > self.length() - 1:
            return print("ERROR: 'update_node' Index out of range of linked list!")
        current_node = self.head
        search_index = 0
        # Продолжайте проходить через узлы до нужного узла
        while search_index != index:
            current_node = current_node.next
            search_index += 1
        # Теперь можно изменить данные
        current_node.data = new_data

    def get_node_data(self, index):
        '''Извлекает эти данные из определенного узла через index'''
        # Связанный список пуст
        if self.is_empty_list():
            return print("ERROR: 'get_node_data' Linked list is empty; no node data to update!")
        # Необходимо убедиться, что индекс находится в правильном диапазоне
        elif index < 0:
            return print("ERROR: 'get_node_data' Index cannot be negative!")
        elif index > self.length() - 1:
            return print("ERROR: 'get_node_data' Index out of range of linked list!")
        # Индекс соответствует HEAD или END
        if index == 0:
            return self.head.data
        elif index == self.length() - 1:
            return self.end.data
        current_node = self.head
        search_index = 0
        # Продолжайте проходить через узлы до нужного узла
        while search_index != index:
            current_node = current_node.next
            search_index += 1
        print(current_node.data)

    def get_list_data(self):
        '''Извлекаем все данные'''
        if self.is_empty_list():
            return "[]"
        result = '['
        current_node = self.head
        while current_node is not None:
            result += f"'{current_node.data}', "
            current_node = current_node.next
        result = result[:-2] + ']'
        print(result)


if __name__ == "__main__":
    obj = LinkedList()
    obj.queue_append('car')
    obj.queue_append('apple')
    obj.queue_append('ball')
    obj.get_list_data()                 # ['car', 'apple', 'ball']
    obj.stack_insert('cake')
    obj.get_list_data()                 # ['cake', 'car', 'apple', 'ball']
    obj.get_node_data(1)                # car
    obj.update_node('pear', 2)
    obj.delete_node(0)
    obj.get_list_data()                 # ['car', 'pear', 'ball']
    obj.delete_end()
    obj.get_list_data()                 # ['car', 'pear']
    obj.insert_after('dog', 0)
    obj.get_list_data()                 # ['car', 'dog', 'pear']
    print(obj.length())                 # 3
